# TinySegmenter Lua 5.3移植

[TinySegmenter](http://chasen.org/~taku/software/TinySegmenter/)のLua 5.3移植です。 5.2以下ではチェックしていません。ただ、5.1/5.2用のutf8モジュールがあれば動くと思います。

## Usage

```
#!lua

local tinyseg = require "tinyseg"

for k, v in pairs( tinyseg.segment( "今日は晴れだった。" ) ) do
	print( k, v )
end
```

## License

元のJavaScript版のLicenseテキストをご覧ください

[http://chasen.org/~taku/software/TinySegmenter/LICENCE.txt](http://chasen.org/~taku/software/TinySegmenter/LICENCE.txt)